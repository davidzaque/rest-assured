package APIRest;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import org.json.*;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.*;

public class TestAPI {

    @Test
    public void TestePostman() {

        String uriBase = "https://postman-echo.com/get";
        given()
                .relaxedHTTPSValidation()
                .param("foo1", "bar1")
                .param("foo2", "bar2")
                .when()
                .get(uriBase)
                .then()
                .statusCode(200)
                .contentType(ContentType.JSON)
                .body("headers.host", is("postman-echo.com"))
                .body("args.foo1", containsString("bar"));
    }

    @Test
    public void TestFullBodyMatchReqresPage() {
        String uriBase = "https://reqres.in", partUri = "/api/users/2";
        String str = "{\"data\":{\"id\":2,\"email\":\"janet.weaver@reqres.in\",\"first_name\":\"Janet\",\"last_name\":\"Weaver\",\"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/josephstein/128.jpg\"}}";
        JSONObject my_obj = new JSONObject(str);

        given()
                .when()
                .get(uriBase + partUri)
                .then()
                .time(lessThan(2000L))
                .statusCode(200)
                .assertThat()
                .body("data.first_name", equalTo(my_obj.getJSONObject("data").getString("first_name")),
                        "data.last_name", equalTo(my_obj.getJSONObject("data").getString("last_name")));
    }

    @Test
    public void TestReqresSingleUser() {

        String uriBase = "https://reqres.in", partUri = "/api/users/";
        given()
                .contentType(ContentType.JSON)
                .pathParam("id", "2")
                .when()
                .get(uriBase + partUri + "{id}")
                .then()
                .statusCode(200)
                .body("data.first_name", equalTo("Janet"));
    }

    @Test
    public void TestReqresListUsers() {

        String uriBase = "https://reqres.in/api/users?page=";
        given()
                .contentType(ContentType.JSON)
                .pathParam("id", "2")
                .when()
                .get(uriBase + "{id}")
                .then()
                .statusCode(200)
                .body("data.first_name", hasItems("Michael", "Lindsay"));
    }

    @Test
    public void httpPostReqresMethodCreateUser() throws InterruptedException {

        //Rest API's URL
        String restAPIUrl = "https://reqres.in/api/users";

        //API Body
        String apiBody = "{\"name\":\"David\",\"job\":\"QA\"}";

        // Building request by using requestSpecBuilder
        RequestSpecBuilder builder = new RequestSpecBuilder();

        //Set API's Body
        builder.setBody(apiBody);

        //Setting content type as application/json
        builder.setContentType("application/json; charset=UTF-8");

        RequestSpecification requestSpec = builder.build();

        //Making post request
        Response response = given().spec(requestSpec).when().post(restAPIUrl);

        JsonPath JSONResponseBody = new JsonPath(response.body().asString());

        //Get the desired value of a parameter
        String result = JSONResponseBody.getString("job");

        //Check the Result
        Assert.assertEquals(result, "QA");
    }

    @Test
    public void httpPostReqresMethodLogin() throws InterruptedException {

        //Rest API's URL
        String restAPIUrl = "https://reqres.in/api/login";

        //API Body
        String apiBody = "{\"email\":\"eve.holt@reqres.in\",\"password\":\"cityslicka\"}";

        // Building request by using requestSpecBuilder
        RequestSpecBuilder builder = new RequestSpecBuilder();

        //Set API's Body
        builder.setBody(apiBody);

        //Setting content type as application/json
        builder.setContentType("application/json; charset=UTF-8");

        RequestSpecification requestSpec = builder.build();

        //Making post request
        Response response = given().spec(requestSpec).when().post(restAPIUrl);

        JsonPath JSONResponseBody = new JsonPath(response.body().asString());

        //Get the desired value of a parameter
        String result = JSONResponseBody.getString("token");

        //Check the Result
        Assert.assertEquals(result, "QpwL5tke4Pnpja7X4");
    }

    @Test
    public void httpDeleteReqresMethodUser() throws InterruptedException {

        //Rest API's URL
        String restAPIUrl = "https://reqres.in/api/users";

        //API Body
        String apiBody = "{\"name\":\"David\",\"job\":\"leader\"}";

        // Building request by using requestSpecBuilder
        RequestSpecBuilder builder = new RequestSpecBuilder();

        //Set API's Body
        builder.setBody(apiBody);

        //Setting content type as application/json
        builder.setContentType("application/json; charset=UTF-8");

        RequestSpecification requestSpec = builder.build();

        //Making delete request
        given().spec(requestSpec).when().delete(restAPIUrl + "2")
                .then()
                .statusCode(204);
    }

    @Test
    public void httpPutReqresMethodUser() throws InterruptedException {

        //Rest API's URL
        String restAPIUrl = "https://reqres.in/api/users";

        //API Body
        String apiBody = "{\"name\":\"David\",\"job\":\"lider\"}";

        // Building request by using requestSpecBuilder
        RequestSpecBuilder builder = new RequestSpecBuilder();

        //Set API's Body
        builder.setBody(apiBody);

        //Setting content type as application/json
        builder.setContentType("application/json; charset=UTF-8");

        RequestSpecification requestSpec = builder.build();

        //Making post request
        Response response = given().spec(requestSpec).when().put(restAPIUrl);

        JsonPath JSONResponseBody = new JsonPath(response.body().asString());

        //Get the desired value of a parameter
        String result = JSONResponseBody.getString("job");

        //Check the Result
        Assert.assertEquals(result, "lider");
    }

    @Test
    public void httpPatchReqresMethodUser() throws InterruptedException {

        //Rest API's URL
        String restAPIUrl = "https://reqres.in/api/users";

        //API Body
        String apiBody = "{\"name\":\"David\",\"job\":\"lider\"}";

        // Building request by using requestSpecBuilder
        RequestSpecBuilder builder = new RequestSpecBuilder();

        //Set API's Body
        builder.setBody(apiBody);

        //Setting content type as application/json
        builder.setContentType("application/json; charset=UTF-8");

        RequestSpecification requestSpec = builder.build();

        //Making post request
        Response response = given().spec(requestSpec).when().patch(restAPIUrl);

        JsonPath JSONResponseBody = new JsonPath(response.body().asString());

        //Get the desired value of a parameter
        String result = JSONResponseBody.getString("job");

        //Check the Result
        Assert.assertEquals(result, "lider");
    }

}
